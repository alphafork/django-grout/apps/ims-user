from dj_rest_auth.views import PasswordChangeView
from django.contrib.auth.models import Group, User
from ims_api_permission.apis import APIMethodPermission
from ims_base.apis import BaseAPIView, BaseAPIViewSet, BaseRetrieveView
from rest_framework.filters import BaseFilterBackend
from rest_framework.generics import CreateAPIView, RetrieveAPIView, get_object_or_404
from rest_framework.response import Response

from ims_user.models import Contact
from ims_user.serializers import (
    UserAccountSerializer,
    UserAddressSerializer,
    UserPasswordResetSerializer,
    UserPasswordSerializer,
    UserProfileSerializer,
)


class UserFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        obj_user_groups = getattr(view, "obj_user_groups", [])
        if (
            not request.user.is_superuser
            and not request.user.groups.filter(name__in=obj_user_groups).exists()
        ):
            user_lookup_field = getattr(view, "user_lookup_field", "user")
            queryset = queryset.filter(**{user_lookup_field: request.user.id})
        return queryset


class BaseUserAPI(BaseAPIViewSet):
    filter_backends = BaseAPIViewSet.filter_backends + [UserFilter]
    obj_user_groups = ["Admin", "Manager", "Accountant"]
    managed_permissions = {
        "OPTIONS": ["Student", "Staff"],
        "GET": ["Student", "Staff"],
        "POST": ["Admin"],
        "PUT": ["Student", "Staff"],
        "PATCH": ["Student", "Staff"],
    }
    permission_classes = BaseAPIViewSet.permission_classes + [APIMethodPermission]


class UserAddressAPI(BaseRetrieveView):
    queryset = Contact.objects.all()
    model_class = Contact
    filter_backends = BaseAPIViewSet.filter_backends + [UserFilter]
    obj_user_groups = ["Admin", "Manager", "Accountant"]
    managed_permissions = {
        "OPTIONS": ["Student", "Staff"],
        "GET": ["Student", "Staff"],
        "POST": ["Admin"],
        "PUT": ["Student", "Staff"],
        "PATCH": ["Student", "Staff"],
    }
    permission_classes = BaseAPIView.permission_classes + [APIMethodPermission]
    serializer_class = UserAddressSerializer


class UserGroupsAPI(RetrieveAPIView, BaseAPIView):
    def get(self, request, **kwargs):
        return Response(Group.objects.values())


class UserDetailAPI(BaseAPIView):
    def get(self, request, **kwargs):
        user = get_object_or_404(User, id=request.user.id)
        data = {
            "id": user.id,
            "username": user.username,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "email": user.email,
            "is_superuser": user.is_superuser,
            "last_login": user.last_login,
        }
        return Response(data)


class UserProfileAPI(BaseAPIViewSet):
    http_method_names = ["get"]
    serializer_class = UserProfileSerializer
    queryset = User.objects.all()
    filter_backends = BaseAPIViewSet.filter_backends + [
        UserFilter,
    ]
    user_lookup_field = "id"
    model_class = User


class UserAccountAPI(BaseUserAPI):
    model_class = User
    serializer_class = UserAccountSerializer
    user_lookup_field = "id"


class UserPasswordResetAPI(CreateAPIView, BaseAPIView):
    serializer_class = UserPasswordResetSerializer
    managed_permissions = {
        "OPTIONS": ["Admin"],
        "GET": ["Admin"],
        "POST": ["Admin"],
        "PUT": ["Admin"],
        "PATCH": ["Admin"],
    }
    permission_classes = [APIMethodPermission]


class UserPasswordAPI(PasswordChangeView):
    serializer_class = UserPasswordSerializer

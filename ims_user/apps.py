from django.apps import AppConfig


class IMSUserConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_user"

    model_strings = {
        "CONTACT": "Contact",
        "PROFILE": "Profile",
        "HONORIFIC": "Honorific",
        "GENDER": "Gender",
    }

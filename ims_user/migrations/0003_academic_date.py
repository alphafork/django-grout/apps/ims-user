# Generated by Django 4.1.7 on 2024-01-22 11:46

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("ims_user", "0002_work_academic_exp"),
    ]

    operations = [
        migrations.AlterField(
            model_name="academic",
            name="completion_date",
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name="academic",
            name="join_date",
            field=models.DateField(blank=True, null=True),
        ),
    ]

from django.contrib.auth.models import User
from django.db import models
from django.db.models.base import post_save
from django.dispatch import receiver
from ims_base.models import AbstractBaseDetail, AbstractLog


class Honorific(AbstractBaseDetail):
    pass


class Gender(AbstractBaseDetail):
    pass


class Profile(AbstractLog):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    honorific = models.ForeignKey(
        Honorific, on_delete=models.CASCADE, null=True, blank=True
    )
    date_of_birth = models.DateField(null=True, blank=True)
    gender = models.ForeignKey(Gender, on_delete=models.CASCADE, null=True, blank=True)
    brief_bio = models.TextField(null=True, blank=True)
    photo = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.user.get_full_name()


class Contact(AbstractLog):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    primary_phone_no = models.CharField(max_length=12, null=True, blank=True)
    secondary_phone_no = models.CharField(max_length=12, null=True, blank=True)
    current_address = models.TextField(null=True, blank=True)
    current_postalcode = models.PositiveIntegerField(null=True, blank=True)
    permanent_address = models.TextField(null=True, blank=True)
    permanent_postalcode = models.PositiveIntegerField(null=True, blank=True)
    personal_website = models.URLField(null=True, blank=True)
    is_verified = models.BooleanField(default=False)

    def __str__(self):
        return self.user.get_full_name()


class Academic(AbstractLog):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    institute_name = models.CharField(max_length=255, null=True, blank=True)
    university_board = models.CharField(max_length=255, null=True, blank=True)
    course = models.CharField(max_length=255)
    join_date = models.DateField(null=True, blank=True)
    completion_date = models.DateField(null=True, blank=True)
    guide = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return self.course


class WorkExperience(AbstractLog):
    employer = models.CharField(max_length=255)
    designation = models.CharField(max_length=255)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.designation


@receiver(post_save, sender=User)
def add_profile_contact(instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
        Contact.objects.create(user=instance)

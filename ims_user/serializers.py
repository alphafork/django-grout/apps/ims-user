import datetime

from dj_rest_auth.registration.serializers import RegisterSerializer
from dj_rest_auth.serializers import PasswordChangeSerializer
from django.contrib.auth.models import User
from ims_base.serializers import BaseModelSerializer
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from ims_user.models import Contact, Gender, Honorific, Profile

from .services import get_contact_data, get_profile_data


class UserRegistrationSerializer(RegisterSerializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()
    honorific = serializers.PrimaryKeyRelatedField(
        write_only=True,
        required=False,
        queryset=Honorific.objects.all(),
        allow_null=True,
    )
    gender = serializers.PrimaryKeyRelatedField(
        write_only=True,
        required=False,
        queryset=Gender.objects.all(),
        allow_null=True,
    )
    date_of_birth = serializers.DateField(write_only=True, required=False)
    brief_bio = serializers.CharField(write_only=True, required=False)
    phone_no = serializers.IntegerField(write_only=True)
    current_address = serializers.CharField(write_only=True)
    current_postalcode = serializers.IntegerField(write_only=True)

    class Meta:
        model = User
        fields = []
        extra_meta = {
            "password1": {
                "type": "password",
                "label": "Password",
            },
            "password2": {
                "type": "password",
                "label": "Confirm password",
            },
            "honorific": {
                "related_model_url": "/user/honorific",
            },
            "gender": {
                "related_model_url": "/user/gender",
            },
        }

    def get_extra_meta(self):
        return self.Meta.extra_meta

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        date_of_birth = validated_data.get("date_of_birth", None)
        brief_bio = validated_data.get("brief_bio", None)
        phone_no = validated_data.get("phone_no", None)
        current_address = validated_data.get("current_address", None)
        current_postalcode = validated_data.get("current_postalcode", None)
        if date_of_birth and date_of_birth > datetime.date.today():
            raise ValidationError("Add correct birth date.")
        if brief_bio and len(str(brief_bio)) > 300:
            raise ValidationError("Character count should be less than 300.")
        if len(str(phone_no)) > 13:
            raise ValidationError("Phone number should be less than 13 characters.")
        if Contact.objects.filter(primary_phone_no=phone_no).exists():
            raise ValidationError("User with this phone number already exists.")
        if len(str(current_address)) > 300:
            raise ValidationError("Character count should be less than 300.")
        if len(str(current_postalcode)) != 6:
            raise ValidationError("Pincode must be six character length.")
        return validated_data

    def get_cleaned_data(self):
        cleaned_data = super().get_cleaned_data()
        cleaned_data.update(
            {
                "first_name": self.validated_data.get("first_name", ""),
                "last_name": self.validated_data.get("last_name", ""),
            }
        )
        return cleaned_data

    def save(self, request):
        honorific = self.validated_data.pop("honorific", None)
        gender = self.validated_data.pop("gender", None)
        date_of_birth = self.validated_data.pop("date_of_birth", None)
        phone_no = self.validated_data.pop("phone_no", None)
        current_address = self.validated_data.pop("current_address", None)
        current_postalcode = self.validated_data.pop("current_postalcode", None)

        user = super().save(request)

        Profile.objects.update_or_create(
            user=user,
            defaults={
                "honorific_id": honorific,
                "date_of_birth": date_of_birth,
                "gender_id": gender,
            },
        )
        Contact.objects.update_or_create(
            user=user,
            defaults={
                "primary_phone_no": phone_no,
                "current_address": current_address,
                "current_postalcode": current_postalcode,
            },
        )
        return user


class UserAccountSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        excluded_fields = [
            "password",
            "last_login",
            "is_superuser",
            "is_staff",
            "is_active",
            "date_joined",
            "groups",
            "user_permissions",
        ] + BaseModelSerializer.Meta.excluded_fields

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["name"] = instance.get_full_name()
        return data


class UserProfileSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        model = User

    def to_representation(self, instance):
        return {
            **get_profile_data(instance),
            **get_contact_data(instance),
        }


class UserPasswordSerializer(PasswordChangeSerializer):
    class Meta:
        extra_meta = {
            "old_password": {
                "type": "password",
                "label": "Old Password",
            },
            "new_password1": {
                "type": "password",
                "label": "New Password",
            },
            "new_password2": {
                "type": "password",
                "label": "Confirm Password",
            },
        }

    def get_extra_meta(self):
        return self.Meta.extra_meta


class UserAddressSerializer(BaseModelSerializer):
    class Meta(BaseModelSerializer.Meta):
        model = Contact

    def to_representation(self, instance):
        data = super().to_representation(instance)
        address = [
            instance.user.get_full_name(),
            instance.current_address,
            instance.current_postalcode,
            instance.primary_phone_no,
        ]
        data["address"] = "\n".join(str(e) for e in address if e)
        return data


class UserPasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField(write_only=True)
    password = serializers.CharField(write_only=True, style={"input_type": "password"})
    confirm_password = serializers.CharField(
        write_only=True, style={"input_type": "password"}
    )

    class Meta:
        fields = ["email", "password", "confirm_password"]
        extra_meta = {
            "password": {
                "type": "password",
            },
            "confirm_password": {
                "type": "password",
            },
        }

    def get_extra_meta(self):
        return self.Meta.extra_meta

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        email = validated_data.get("email")
        if not User.objects.filter(email=email).exists():
            raise serializers.ValidationError({"email": "Email not found."})
        password = validated_data.get("password")
        confirm_password = validated_data.pop("confirm_password")
        if password != confirm_password:
            raise serializers.ValidationError(
                {"confirm_password": "Passwords do not match."}
            )
        return validated_data

    def save(self):
        email = self.validated_data.pop("email")
        user = User.objects.get(email=email)
        user.set_password(self.validated_data["password"])
        user.save()
        return user

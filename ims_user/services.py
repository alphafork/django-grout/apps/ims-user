def get_profile_data(user):
    profile_data = {
        "user_id": user.id,
        "full_name": user.get_full_name(),
        "first_name": user.first_name,
        "last_name": user.last_name,
        "email": user.email,
        "last_login": user.last_login,
        "date_of_birth": None,
        "brief_bio": "",
        "photo": None,
        "honorific": None,
        "gender": None,
    }
    profile = getattr(user, "profile", None)
    if profile:
        profile_data.update(
            {
                "date_of_birth": user.profile.date_of_birth,
                "brief_bio": user.profile.brief_bio,
                "honorific": f"{user.profile.honorific}",
                "gender": f"{user.profile.gender}",
            }
        )
        photo = user.profile.photo
        if photo:
            profile_data.update({"photo": photo.path})
    return profile_data


def get_contact_data(user):
    contact_data = {
        "primary_phone_no": "",
        "secondary_phone_no": "",
        "current_address": "",
        "current_postalcode": None,
        "permanent_address": "",
        "permanent_postalcode": None,
        "personal_website": "",
    }
    contact = getattr(user, "contact", None)
    if contact:
        contact_data.update(
            {
                "primary_phone_no": user.contact.primary_phone_no,
                "secondary_phone_no": user.contact.secondary_phone_no,
                "current_address": user.contact.current_address,
                "current_postalcode": user.contact.current_postalcode,
                "permanent_address": user.contact.permanent_address,
                "permanent_postalcode": user.contact.permanent_postalcode,
                "personal_website": user.contact.personal_website,
            }
        )
    return contact_data

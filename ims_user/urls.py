from django.urls import include, path
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from .apis import (
    BaseUserAPI,
    UserAccountAPI,
    UserAddressAPI,
    UserDetailAPI,
    UserGroupsAPI,
    UserPasswordAPI,
    UserPasswordResetAPI,
    UserProfileAPI,
)

router = routers.SimpleRouter()
router.register(r"profile", BaseUserAPI, "user-profile")
router.register(r"contact", BaseUserAPI, "user-contact")
router.register(r"account", UserAccountAPI, "user-account")
router.register(r"user-profile", UserProfileAPI, "user-profile")

urlpatterns = [
    path("address/<int:pk>/", UserAddressAPI.as_view()),
    path("detail/", UserDetailAPI.as_view()),
    path("groups/", UserGroupsAPI.as_view()),
    path("change-password/", UserPasswordAPI.as_view()),
    path("reset-password/", UserPasswordResetAPI.as_view()),
    path("", include(router.urls)),
]

urlpatterns = format_suffix_patterns(urlpatterns, allowed=["json"])
